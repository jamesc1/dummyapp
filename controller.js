var app = angular.module('app', ['ui.router'])

app.config(['$stateProvider', function($stateProvider){
    $stateProvider.state('root',{
        url: '/',
        template: '<h1>Hello World</h1>',
        controller: ''
    })
    .state('viewState',{
      url: '/view',
      templateUrl: 'view.html',
      controller: 'viewState'
    })
    .state('addState',{
      url: '/add',
      template: '<h2>World</h2>{{currentLoc}}',
      controller: 'addState'
    })
    .state('noroute',{
      url: '*path',
      template: '<h2>Goodbye</h2>{{currentLoc}}',
    })
}]);

app.controller('viewState', function($scope){
  $scope.currentLoc = 'View Page'
});

app.controller('addState', function($scope){
  $scope.currentLoc = 'Add Page'
})